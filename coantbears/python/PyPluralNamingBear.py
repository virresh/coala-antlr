"""PyPluralNamingBear module."""

import inflect

from coantlib.ASTBear import ASTBear
from coantlib.NodeData import NodeData
from coalib.results.Diff import Diff
from coalib.results.Result import Result
from coalib.results.TextRange import TextRange
from dependency_management.requirements.PipRequirement import PipRequirement


def get_other(val):
    """
    Fetch ``NodeData.other`` in a nested sequence of ``NodeData``.

    For e.g

    .. code:: Python

       data = {
           [
               (
                   NodeData('1'),
                   NodeData('2'),
               ),
               (
                   NodeData('3'),
               )
           ]
       }

    In ``data`` as above, this function will retrieve the ``other`` field for
    ``NodeData('1')``.
    """
    while not isinstance(val, NodeData):
        val = val[0]
    return val.other


def needs_to_be_plural(param, val, ignore_list):
    """
    Check if a variable needs to be plural.

    :param param:           NodeData of the variable to check
    :param val:             NodeData or Sequence of NodeData ``param``
                            is assigned to.
    :param ignore_list:     A list of words that should be ignored.
    """
    engine = inflect.engine()
    if not isinstance(val, tuple):
        return False

    if str(param) in ignore_list:
        return False

    var_name = str(param).split('.')[-1].split('[')[0]

    if var_name.startswith('__'):
        return False
    elif not engine.singular_noun(var_name) is False:
        return False

    common_match_words = ['list', 'dict', 'map', 'set']
    for word in common_match_words:
        if var_name.casefold().endswith(word):
            return False

    for subword in var_name.split('_'):
        if (subword and
                subword not in ignore_list and
                engine.singular_noun(subword)):
            return False

    return True


def get_plural(lhs, rhs):
    """Make a word plural if possible."""
    engine = inflect.engine()
    inflected_word = engine.plural(lhs.text)
    if not needs_to_be_plural(inflected_word, rhs, []):
        return inflected_word
    return None


def get_diff(var, value, orig_diff):
    """Generate diff when ``var`` is changed into plural."""
    diff = Diff(orig_diff.original)
    diff_range = TextRange.from_values(
        var.lineNo, var.colNo, var.lineNo, var.colNo+len(var.text))
    new_var = get_plural(var, value)
    if not new_var:
        return None
    diff.replace(diff_range, new_var)
    return diff


class PyPluralNamingBear(ASTBear):
    """
    Checks all sequences to be plural.

    This bear will check if a variable is assigned to a list or dictionary,
    then mandate the variable to be plural.
    """

    LANGUAGES = {'Python 3'}
    REQUIREMENTS = {PipRequirement('inflect', '0.3.1')}
    CAN_FIX = {'Syntax'}

    def run(self,
            filename,
            file,
            ignore_list: list = [],
            disable_default_list: bool = False,
            ):
        """
        Enforce plural naming for lists and dictionaries.

        :param ignore_list:             A list of keywords to ignore.
        :param disable_default_list:    Turn off default list of ignore 
                                        keywords.
        """
        super().run(filename, file)
        diff = Diff(file)
        if not disable_default_list:
            ignore_list += ['all', 'data', 'result', 'retval']
        for function, value in self.walker.get_functions().items():
            if (value['*'] is not None and
                    needs_to_be_plural(str(value['*']), tuple(), ignore_list)):
                n_diff = get_diff(value['*'], tuple(), diff)
                yield Result.from_values(
                            origin=self,
                            message=('Function: {} should use plural args '
                                     'rather than *{}'.format(
                                                        function,
                                                        value['*'].text
                                                        )
                                     ),
                            file=filename,
                            line=value['*'].lineNo,
                            column=value['*'].colNo,
                            end_line=value['*'].lineNo,
                            end_column=value['*'].colNo+len(str(value['*'])),
                            diffs={filename: n_diff},
                            )
            if (value['**'] is not None and
                    needs_to_be_plural(str(value['**']), tuple(), ignore_list)):
                n_diff = get_diff(value['**'], tuple(), diff)
                yield Result.from_values(
                            origin=self,
                            message=('Function: {} should use plural kwargs '
                                     'rather than **{}'.format(
                                                        function,
                                                        value['**'].text
                                                        )
                                     ),
                            file=filename,
                            line=value['**'].lineNo,
                            column=value['**'].colNo,
                            end_line=value['**'].lineNo,
                            end_column=value['**'].colNo+len(str(value['**'])),
                            diffs={filename: n_diff},
                            )

        for (var, value) in self.walker.get_assignments():
            # important ---> tuple check only if it passes the first check
            if (needs_to_be_plural(var, value, ignore_list) and
                    get_other(value) != '()'):
                n_diff = get_diff(var, value, diff)
                yield Result.from_values(
                        origin=self,
                        message=('Sequence {} should be given a name '
                                 'that is plural'.format(var.text)),
                        file=filename,
                        line=var.lineNo,
                        column=var.colNo,
                        end_line=var.lineNo,
                        end_column=(var.colNo+len(var.text)),
                        diffs={filename: n_diff},
                        )
